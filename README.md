# CBAS Metatranscriptome

This repository is a companion of our preprint:

  - Sergio Vargas, Ramón E. Rivera-Vicéns, Michael Eitel, Gabrielle Büttner, Gert Wörheide. 2022. rRNA depletion for holobiont metatranscriptome profiling across demosponges. bioRxiv. XXXX.

please cite this preprint if using the data, assemblies or scripts provided here.

### Folder structure

```
CBAS_Metatranscriptome/
			+-- README.md
			+-- LICENSE
			+- Assemblies/
					+- polyA: TransPi assemblies for CBAS (*Lendenfeldia chondrodes*) and *Tethya wilhelma* polyA-selected RNASeq data.
					+- rRNA_Depleted: TransPi assemblies for CBAS (*Lendenfeldia chondrodes*) and *Tethya wilhelma* rRNA-depleted RNASeq data.
			+- Bacterial_Genomes: Actinobacterial and cyanobacterial genes used for differential (bacterial) gene expression analyses in CBAS.
			+- Counts: Actinobacterial and cyanobacterial gene counts used for differential (bacterial) gene expression analyses with DESeq2.
			+- Outfiles: outfiles produced using different R Scripts for the project.
			+- R: R scripts used for the project.

```

If you want to check and use TransPi, you can check this great tool [here](https://github.com/PalMuc/TransPi) :-)


#### NOTES

1. This repository (or parts of it) may change in the future. Check the releases section to see if there are snap-shots of it.

2. This repository is **public** and the following personal information is visible:
	* After each **commit*, the name and E-mail address as saved in your Git config.
	* The **GitLab username* as saved in your user settings (under Account).

3. In German: Bitte beachten Sie: Bei öffentlichen Projekten sind die folgenden Informationen öffentlich sichtbar (allgemein zugreifbar):
	* Bei jedem Commit der Name und die E-Mail-Adresse, wie sie in der Konfiguration von Git hinterlegt wurden.
	* Bei GitLab der GitLab-Benutzername (Username), wie er bei den Benutzereinstellungen (User Settings) im Abschnitt "Konto" (Account) festgelegt wird. Der Benutzername ist bei persönlichen Projekten im Pfad sichtbar.

#### Note that by commiting to this repository you accept the publication of the information detailed above. More information about public repositories can be find here: https://doku.lrz.de/display/PUBLIC/GitLab


Sergio



<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
