29905,Amphimedon queenslandica (Sponge)
1807,Stylophora pistillata (Smooth cauliflower coral)
1493,Nephila clavipes (Golden silk orbweaver)
1227,Cnidaria
1171,Lingula unguis
1076,Nematostella vectensis (Starlet sea anemone)
1075,Branchiostoma floridae (Florida lancelet) (Amphioxus)
842,Bacillus sp. BAC-SubDo-3
753,Bilateria
639,Teleostei
610,Metazoa
576,Strongylocentrotus purpuratus (Purple sea urchin)
467,Crustacea
451,Chiloscyllium punctatum (Brownbanded bambooshark) (Hemiscyllium punctatum)
450,Stichopus japonicus (Sea cucumber)
429,Capitella teleta (Polychaete worm)
389,Mizuhopecten yessoensis (Japanese scallop) (Patinopecten yessoensis)
356,Acari
343,EUMVA
334,Crassostrea gigas (Pacific oyster) (Crassostrea angulata)
