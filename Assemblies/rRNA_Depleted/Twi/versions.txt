==========================================
TransPi - Transcriptome Analysis Pipeline
==========================================

		RUN INFO

-- Kmers used --
25,33,37,45

-- Databases name and last update --
Uniprot_DB: uniprot_metazoa_33208.fasta
Uniprot_DB last update: Thu 09 Apr 2020 02:43:15 PM UTC 

PfamA last update: Thu 09 Apr 2020 02:39:46 PM UTC 

BUSCO_v3_DB: metazoa_odb9
BUSCO_v4_DB: metazoa_odb10

-- Program versions --
SOAP: 1.03
Velveth: 1.2.10
Velvetg: 1.2.10
Oases: 0.2.09
rna-SPADES: SPAdes genome assembler v3.14.0 [rnaSPAdes mode]
Trans-ABySS: 2.0.1
Trinity: v2.9.1
Diamond: 0.9.30
HMMER: 3.3
EvidentialGene: 2019.05.14
RNAmmer: 1.2
Transdecoder: 5.5.0
BUSCO3: 3.0.2
BUSCO4: 4.0.5
Trinotate: 3.2.0
SignalP: 4.1
tmhmm: 2.0
CD-HIT: 4.8.1 
Exonerate: 2.4.0

-- Programming Languages --
R: 3.6.2
Python: 3.6.10
Perl: 5.26.2
