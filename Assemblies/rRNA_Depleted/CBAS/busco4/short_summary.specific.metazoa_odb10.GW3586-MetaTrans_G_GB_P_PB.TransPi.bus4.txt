# BUSCO version is: 4.0.5 
# The lineage dataset is: metazoa_odb10 (Creation date: 2019-11-20, number of species: 65, number of BUSCOs: 954)
# Summarized benchmarking in BUSCO notation for file GW3586-MetaTrans_G_GB_P_PB.combined.okay.fa
# BUSCO was run in mode: transcriptome

	***** Results: *****

	C:73.6%[S:59.6%,D:14.0%],F:13.2%,M:13.2%,n:954	   
	703	Complete BUSCOs (C)			   
	569	Complete and single-copy BUSCOs (S)	   
	134	Complete and duplicated BUSCOs (D)	   
	126	Fragmented BUSCOs (F)			   
	125	Missing BUSCOs (M)			   
	954	Total BUSCO groups searched		   
