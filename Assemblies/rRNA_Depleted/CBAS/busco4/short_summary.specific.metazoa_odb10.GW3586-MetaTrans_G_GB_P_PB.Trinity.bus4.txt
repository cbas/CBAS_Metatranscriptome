# BUSCO version is: 4.0.5 
# The lineage dataset is: metazoa_odb10 (Creation date: 2019-11-20, number of species: 65, number of BUSCOs: 954)
# Summarized benchmarking in BUSCO notation for file GW3586-MetaTrans_G_GB_P_PB.Trinity.fa
# BUSCO was run in mode: transcriptome

	***** Results: *****

	C:66.2%[S:33.2%,D:33.0%],F:19.6%,M:14.2%,n:954	   
	632	Complete BUSCOs (C)			   
	317	Complete and single-copy BUSCOs (S)	   
	315	Complete and duplicated BUSCOs (D)	   
	187	Fragmented BUSCOs (F)			   
	135	Missing BUSCOs (M)			   
	954	Total BUSCO groups searched		   
