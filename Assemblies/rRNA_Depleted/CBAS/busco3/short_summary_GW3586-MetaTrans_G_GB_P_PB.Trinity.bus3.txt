# BUSCO version is: 3.0.2 
# The lineage dataset is: metazoa_odb9 (Creation date: 2016-02-13, number of species: 65, number of BUSCOs: 978)
# To reproduce this run: python /home/rrivera/anaconda3/envs/TransPi2/bin/run_BUSCO.py -i GW3586-MetaTrans_G_GB_P_PB.Trinity.fa -o GW3586-MetaTrans_G_GB_P_PB.Trinity.bus3 -l /home/rrivera/TransPi/DBs/busco_db/metazoa_odb9/ -m transcriptome -c 8
#
# Summarized benchmarking in BUSCO notation for file GW3586-MetaTrans_G_GB_P_PB.Trinity.fa
# BUSCO was run in mode: transcriptome

	C:74.7%[S:34.6%,D:40.1%],F:17.8%,M:7.5%,n:978

	730	Complete BUSCOs (C)
	338	Complete and single-copy BUSCOs (S)
	392	Complete and duplicated BUSCOs (D)
	174	Fragmented BUSCOs (F)
	74	Missing BUSCOs (M)
	978	Total BUSCO groups searched
