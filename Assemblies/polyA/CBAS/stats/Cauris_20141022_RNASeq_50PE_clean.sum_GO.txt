Summary of Trinotate/Gene Ontologies 

- Individual Cauris_20141022_RNASeq_50PE_clean
	 Total transcripts with GO:
		 11066
	 Total transcripts with only one GO:
		 1282
	 Total transcripts with multiple GO:
		 9784
	 Total GO in the file:
		 137477
	 Total uniq GO in the file:
		 12656
