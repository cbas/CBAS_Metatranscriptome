Summary of BUSCO V4 

-- TransPi BUSCO V4 scores -- 
# BUSCO version is: 4.0.5 
# The lineage dataset is: metazoa_odb10 (Creation date: 2019-11-20, number of species: 65, number of BUSCOs: 954)
# Summarized benchmarking in BUSCO notation for file SRR4255675_Tethya_wilhelma_transcriptome.combined.okay.fa
# BUSCO was run in mode: transcriptome

	***** Results: *****

	C:91.0%[S:61.5%,D:29.5%],F:1.4%,M:7.6%,n:954	   
	868	Complete BUSCOs (C)			   
	587	Complete and single-copy BUSCOs (S)	   
	281	Complete and duplicated BUSCOs (D)	   
	13	Fragmented BUSCOs (F)			   
	73	Missing BUSCOs (M)			   
	954	Total BUSCO groups searched		   

-- Trinity BUSCO V4 scores --
# BUSCO version is: 4.0.5 
# The lineage dataset is: metazoa_odb10 (Creation date: 2019-11-20, number of species: 65, number of BUSCOs: 954)
# Summarized benchmarking in BUSCO notation for file SRR4255675_Tethya_wilhelma_transcriptome.Trinity.fa
# BUSCO was run in mode: transcriptome

	***** Results: *****

	C:91.5%[S:18.1%,D:73.4%],F:1.7%,M:6.8%,n:954	   
	873	Complete BUSCOs (C)			   
	173	Complete and single-copy BUSCOs (S)	   
	700	Complete and duplicated BUSCOs (D)	   
	16	Fragmented BUSCOs (F)			   
	65	Missing BUSCOs (M)			   
	954	Total BUSCO groups searched		   
