
========================================================
    TransPi - Transcriptome Analysis Pipeline v1.0.0-dev
========================================================

			RUN INFO


-- Nextflow info --
Script Id          : 493742c46af95d16ef584190027b11cf
Script name        : TransPi.nf
Script File        : /home/rrivera/test/TransPi/TransPi.nf
Repository         : null
Project directory  : /home/rrivera/test/TransPi
Launch directory   : /home/rrivera/test/TransPi
Work directory     : /home/rrivera/test/TransPi/work_polyA_tethya
Home directory     : /home/rrivera
User name          : rrivera
Config Files       : [/home/rrivera/test/TransPi/nextflow.config]
Container Engine   : null
Cmd line           : nextflow run TransPi.nf --all --maxReadLen 125 --k 25,41,57,67 --outdir Results_polyA_tethya -w work_polyA_tethya --reads '/home/rrivera/test/TransPi/tethya/*_R[1,2].fastq' -profile conda,palmuc --myConda -resume --skipQC
Profile            : conda,palmuc
Run name           : hopeful_raman
Session ID         : a11cdca2-f4d6-4a63-a497-b5c3ec9e5af2

-- Kmers used --
25,41,57,67

-- Databases name and last update --
Uniprot_DB: uniprot_metazoa_33208.fasta
Uniprot_DB last update: Thu 09 Apr 2020 02:43:15 PM UTC 

PfamA last update: Thu 09 Apr 2020 02:39:46 PM UTC 

BUSCO_v4_DB: metazoa_odb10

-- Program versions --

BUSCO4: 4.0.5
Trinity: 2.9.1
Trinotate: 3.2.1
fastp: 0.20.1
rnaQUAST: 2.0.1
Transdecoder: 5.5.0
R: 3.6.3
SOAP: 1.03
EvidentialGene: 2019.05.14
Blast: 2.2.31+
CD-HIT:  4.8.1 
Exonerate: 2.4.0
Bowtie2: 2.3.5.1
Trans-ABySS: 2.0.1
Perl: 5.26.2
Python: 3.6.10
HMMER: 3.3
Diamond: 0.9.30
rna-SPADES: 3.14.0

